<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <title>Audiotext - Transcrição de áudio em texto.<?php //bloginfo('name'); ?> <?php //wp_title(); ?></title>
	



	<meta name="author" content="Audiotext">
    <meta name="keywords" content="Audiotext, Audiotext curitiba, transcrição de audio, transcrição de audio para texto online, transcrição de audio para texto em portugues, transcrição de audio preço, transcrição de audio em texto, transcrição de audio para texto preço, transcrição de audio programa, transcrição de audio para texto, transcrição de audio software, transcrição de audio download, degravação de audio para texto, traducao juramentada">
    <!-- <meta name="description" content=""> -->

	<meta property="og:title" content="Audiotext" />
	<meta property="og:description" content="Audiotext Transcrição de Áudio - Transcrever áudio para texto com profissionais especializados. Confira!" />
	<meta property="og:url" content="http://www.audiotext.com.br/" />
	<meta property="og:image" content="http://www.audiotext.com.br/compartilhar.png"/>
	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="Audiotext" />

	<meta name="twitter:card" content="summary" />
	<meta name="twitter:site" content="http://www.audiotext.com.br" />
	<meta name="twitter:title" content="Audiotext Transcrição de Áudio" />
	<meta name="twitter:description" content="Audiotext Transcrição de Áudio - Transcrever áudio para texto com profissionais especializados. Confira!" />
	<meta name="twitter:image" content="http://www.audiotext.com.br/compartilhar.png" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="canonical" href="http://www.audiotext.com.br/" />
	<link rel="publisher" href="https://plus.google.com/106441247137914216240">
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/img/ico.png">

	
    
    <!-- Apple Touch Icon -->
    <?php if (get_field('apple_touch_icon', 'option')) { ?>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php the_field('apple_touch_icon', 'option'); ?>">
    <?php } ?>

    <!--  Favicon -->
    <?php if (get_field('favicon', 'option')) { ?>
    <link rel="shortcut icon" href="<?php the_field('favicon', 'option'); ?>">
    <?php } ?>

	<script>
	   var hkn = { code: "Audiotext" };

	   (function () {
	       var h = document.createElement('script'); h.type = 'text/javascript'; h.async = true;
	       h.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'tag.hariken.co/hkn.js';
	       var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(h, s);
	   })();
	</script>
	
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67616006-28', 'auto');
  ga('send', 'pageview');

</script>
	
    <?php
    if ( is_singular() && get_option( 'thread_comments' ) ) 	wp_enqueue_script( 'comment-reply' );

    wp_head();
    ?>
</head>

	<body <?php body_class(); ?>>

	<!-- Google Tag Manager
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N5NTMZ"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-N5NTMZ');</script> -->
	<!-- End Google Tag Manager -->

	<!-- CÓDIGO DO GOOGLE PARA TAG DE REMARKETING -->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 995199120;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/995199120/?value=0&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>

	<!-- handtalk - RETIRADO A PEDIDO DO CLIENTE 25/06/2018 -->
	<!-- <script src="//api.handtalk.me/plugin/latest/handtalk.min.js"></script>
	<script>
	  var ht = new HT({
	    token: "dbcf770ff1a5d66ae65745cac334aeee"
	  });
	</script> -->
	<!-- TAG RD STATION -->
	<script type="text/javascript" async src="https://d335luupugsy2.cloudfront.net/js/loader-scripts/d2bd1e3b-d988-48b0-ad21-374dc16370cb-loader.js"></script>

	<!-- Modal CONFIRMAÇÃO CONTATO -->
	<div class="confirmacao-contato">
		<div class="modal" id="modal-confirmacao" role="dialog">

			<button id="btnfecharconfirmacao" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">

						<span class="modal-title">Erro ao enviar mensagem.</span>

					</div>

					<div class="modal-body" >

						<i class="fa fa-times"></i>
						<p>Verifique se os campos foram preenchidos corretamente.</p>
					</div>

				</div>

			</div>

		</div>
	</div>

	<!-- Modal -->
		<div class="modal" id="myModal1" role="dialog" style="display: none;">
		 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

			<div class="modal-dialog modal-lg">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">

						<span class="modal-title" style="display: block;">NÃO ENCONTROU O QUE ESTAVA PROCURANDO ?</span>
						<span style="display: block;">Entre em contato com a <img src="<?php bloginfo('template_directory'); ?>/logo.png" alt="Logotipo da Audiotext" class="logomodal">!</span>
					</div>

					<div class="modal-body" >
						<?php
                           echo do_shortcode('[contact-form-7 id="4893" title="Página Contato"]');
                        ?>

					</div>

				</div>

			</div>

		</div>
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-563cbd2d4c8560a3" async="async"></script>


		<div class="page-container <?php if (get_field('layout_mode', 'option') == "boxed") { echo "page-boxed"; } ?>">

		<div class="preloader"></div>

		<!-- Mobile Navigation
		================================================== -->

		<div class="mobile-nav-container">
			<div class="mobile-nav-bar">
			    <button type="button" class="btn-mobile-nav" data-toggle="collapse" data-target="#mobile-nav"><i class="fa fa-align-justify"></i></button>

				<!-- Mobile Logo -->
				<?php if (get_field('mobile_logo', 'option')) { ?>
				<div class="mobile-logo">
					<a href="<?php echo site_url(); ?>">
						<h1 style="text-indent: -9999em;margin: 0!important;">Audiotext transcrição de audio em texto</h1>
						<img src="<?php the_field('mobile_logo', 'option'); ?>" alt="logo audiotext"/>
					</a>
				</div>
				<?php } else { ?>
				<div class="mobile-logo">
					<a href="<?php echo site_url(); ?>">
						<h1 style="text-indent: -9999em;margin: 0!important;">Audiotext transcrição de audio em texto</h1>
						<img src="<?php the_field('logo_image', 'option'); ?>" alt="logo audiotext"/>
					</a>
				</div>
				<?php } ?>

			</div>

			<!-- Mobile Drop Down -->
			<div id="mobile-nav" class="collapse">
				<ul>
					<?php
					$defaults = array(
				   	'theme_location'  => 'header-menu',
				   	'container'       => 'ul',
				   	'menu_class'      => 'menu',
				   	'echo'            => true,
				   	'fallback_cb'     => 'wp_page_menu',
				   	'items_wrap'      => '%3$s',
				   	'depth'           => 0,
				   	'walker'          => new zt_custom_menu_walker()
				   );
				   ?>
				   <?php wp_nav_menu( $defaults ); ?>
				</ul>
			</div>
		</div>

		<!-- DESKTOP HEADER
		================================================== -->
					<?php
			if (get_field('header_style')) {
				header_style_page();
			}
			?>

			<?php
			if (get_field('header_style') == "default") {
				header_style();
			} else if (get_field('header_layout') == "") {
				header_style();
			}
			?>
		<div class="header">


			<?php
			if (get_field('header_layout')) {
				header_layout_page();
			}
			?>

			<?php
			if (get_field('header_layout') == "default") {
				header_layout();
			} else if (get_field('header_layout') == "") {
				header_layout();
			}
			?>


		</div>
		<!-- RETIRADO A PEDIDO DO CLIENTE 25/06/2018
		 <div class="acessibilidade">
			<ul>
				<li id="botao-aumentar-font"><span>A <i class="fa fa-plus"></i></span> <p>Aumentar fonte</p></li>
				<li id="botao-diminuir-font"><span>A <i class="fa fa-minus"></i></span> <p>Diminuir fonte</p></li>
				<li id="botao-aumentar-contrast"><span><i class="fa fa-desktop"></i> <i class="fa fa-plus"></i></span><p>Contraste</p></li>
				<li id="botao-diminuir-contrast"><span><i class="fa fa-desktop"></i> <i class="fa fa-minus"></i></span><p>Contraste</p></li>
			</ul>
		</div> -->

<!-- 
	<div class="modal fade" tabindex="-1" role="dialog" id="aviso">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<strong>!</strong>
				<span>ATENÇÃO!</span>
				<p>Estamos com problemas em nosso número de Curitiba (41) 3363-3220. <br/> Entre em contato pelo número (41) 99741-0087 ou (41) 99799-0622</p>
				<p>Já estamos trabalhando para restabelecer a linha.</p>
				<p>Desculpe-nos pelo inconveniente temporário.</p>
			</div>
		</div>
	</div> 
 -->
			<div class="geral" style="display: none;" id="formOrcamento">
				<div class="wpb_wrapper" id="textoModalOrcamento">
					<p id="paragrafoModal"class="titulo text-center">ORÇAMENTO TRANSCRIÇÃO DE ÁUDIO</p>
					<p id="paragrafoModal"><span class="text-center" style="display:block">Nos ajude a entender melhor sua demanda preenchendo os campos abaixo.</span></p>
				</div>
				<div class="dimensoes">	
					<div class="row">
						<div class="span6">
							<div class="formularioOrcamentoEsquerda">
								<input type="text" name="nome" placeholder="Nome">
								<input type="text" name="email" placeholder="Email">
								<textarea name="descricaoDemanda" id="textareaDemanda" cols="30" rows="10" placeholder="Resuma aqui um pouco da sua demanda. Isso ajudará nossa equipe a entender melhor seu pedido =)"></textarea>
								<span>Como nos conheceu?</span>
								<div class="selectFormulario">
									<select name="idioma" >
										<option value="">...</option>
										<option value="Já sou cliente">Já sou cliente</option>
										<option value="Google">Google</option>
									</select>
								</div>
								<span>Disponibilidade para ligação:</span>
								<div class="selectFormulario">
									<select name="idioma" >
										<option value="">...</option>
										<option value="manhã">manhã</option>
										<option value="meio dia">meio dia</option>
									</select>
								</div>
								<span class="spanzinho">Atendimento preferencialmente por email.</span>
								<input type="checkbox" name="Atendimentopreferencialmenteporemail[]" value="Atendimento preferencialmente por email.">
							</div>
						</div>
						<div class="span6">
							<div class="formularioOrcamentoEsquerda">
								<input type="text" name="telefone" placeholder="Telefone">
								<input type="text" name="quantidadeMinutos" placeholder="Quantidade de minutos">
								<span>Idioma:</span>
								<div class="selectFormulario">
									<select name="idioma" id="selectIdioma" >
										<option value="">...</option>
										<option value="Português">Português</option>
										<option value="Inglês">Inglês</option>
									</select>
								</div>
								<span>Finalidade:</span>
								<div class="selectFormulario">
									<select name="idioma" >
										<option value="">...</option>
										<option value="Audiências">Audiências</option>
										<option value="Gravações Telefônicas">Gravações Telefônicas</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<input type="submit" name="enviar" value="Enviar" class="bt-enviar">
				</div>
				<span class="fechar" id="fecharOrcamento"></span>
				
			</div>
