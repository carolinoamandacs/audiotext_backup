<?php get_header();?>

<?php
/*-----------------------------------------------------------------------------------*/
/* CONTENT WIDTH SIZE
/*
/* this content width size depends on if the user has selected to display the sidebar
/*-----------------------------------------------------------------------------------*/

$sidebar = get_field('sidebar');

if ($sidebar !== "none") {
	$span_size = "span8";
}
if ($sidebar == "none") {
	$span_size = "span12";
}
if ($sidebar == "") {
	$span_size = "span12";
}
?>

<!-- Slider -->
<div class="layer-slider">
	<?php if (get_field('layerslider_id')) { ?>
		<?php $layerslider_id = get_field('layerslider_id'); ?>
		<?php echo do_shortcode("[layerslider id='$layerslider_id']"); ?>
	<?php } ?>
</div>

<!-- Breadcrumb -->
<?php if (get_field('breadcrumb') == true) { zt_the_breadcrumb(); } ?>

<!-- Modal CAMPANHA -->
<!-- <div class="modal fade campanha" id="modalCampanha" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modalCorpo" role="document">
    <div class="modal-content ">
    <button type="button" class="close closemodalCampanha" data-dismiss="modal" aria-hidden="true">&times;</button>
     	<div class="bgCampanha">
     		
     	</div>	    
    </div>
  </div>
</div> -->

<!-- Begin content -->
<div class="content-container">
	<div class="container">
	
		<div class="row">
		
			<!-- Left sidebar -->
			<?php if ($sidebar == "left") { ?>
				<div class="span4 widget-area widget-left">
					<?php if (get_field('select_a_sidebar')) { ?>
						<?php dynamic_sidebar(get_field('select_a_sidebar')); ?>
					<?php } else { ?>
						<?php dynamic_sidebar('Page'); ?>
					<?php } ?>
				</div>
			<?php } ?>
			
			<!-- MAIN CONTENT -->
			<div class="<?php echo $span_size; ?>">
				
			<?php while (have_posts()) : the_post(); ?>
				<?php $my_terms = get_the_terms( $post->ID, 'Skills' ); ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<?php the_content(); ?>
											
				</div>
			<?php endwhile; ?>
			
			</div>
			
			<!-- Right sidebar -->
			<?php if ($sidebar == "right") { ?>
				<div class="span4 widget-area widget-right">
					<?php if (get_field('select_a_sidebar')) { ?>
						<?php dynamic_sidebar(get_field('select_a_sidebar')); ?>
					<?php } else { ?>
						<?php dynamic_sidebar('Page'); ?>
					<?php } ?>
				</div>
			<?php } ?>
			
		</div>
		
	</div>
</div>

<?php get_footer(); ?>   

<?php //if(is_home()): ?>



    <!-- MODAL DE RECESSO
    <div id="myModal" class="modal fade correcao-x" role="dialog">
      <div class="modal-dialog modal-lg">

        <div class="modal-content">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            <img src="http://www.audiotext.com.br/wp-content/uploads/2015/12/at_recesso.png" class="img-responsive center-block">
        </div>

      </div>
    </div> -->

<script type="text/javascript">
    jQuery(document).ready(function($) {
	if(window.location.href == 'http://www.audiotext.com.br/'){
        	//$('#myModal').modal('show');
	}
    });


</script>

<?php
//endif;
?>