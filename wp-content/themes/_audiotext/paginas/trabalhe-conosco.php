<?php
/**
* Template Name: Trabalhe conosco 
* Description: 
*
* @package audiotext
*/
get_header(); ?>
<title><?php echo get_the_title() ?></title>
<div class="pg pg-entreParaTime">

		<div class="bannerEntreParaTime" style="background:url(<?php echo $configuracao['paginas_entreParaTime_banner']['url'] ?>)" >
			<div class="texto">
				<span><?php echo $configuracao['paginas_entreParaTime_titulo'] ?></span>
				<p><?php echo $configuracao['paginas_entreParaTime_descricao'] ?></p>
			</div>
		</div>

		
		<section class="areaTexto">
			<h6 class="hidden"><?php echo $configuracao['paginas_entreParaTime_conteudo_desc'] ?></h6>
			<?php echo $configuracao['paginas_entreParaTime_conteudo_descricao'] ?>
			<hr>

			<?php if ($configuracao['paginas_entreParaTime_form_hidden'] != "Mostrar"):?>
			<img alt="<?php echo $configuracao['paginas_entreParaTime_conteudo_desc'] ?>" title="<?php echo $configuracao['paginas_entreParaTime_conteudo_desc'] ?>" src="<?php echo $configuracao['paginas_entreParaTime_conteudo_img']['url'] ?>">

			<p><?php echo $configuracao['paginas_entreParaTime_conteudo_desc'] ?></p>
			<?php endif; ?>	
		</section>
		

		<?php if ($configuracao['paginas_entreParaTime_form_hidden'] != "Esconder"):?>
		<div class="formularioOrcamento">
			<?php echo do_shortcode($configuracao['paginas_entreParaTime_formulario']); ?>
		</div>
		<?php endif; ?>	

		<!-- ÁREA DE VAROLES -->
		<?php if ($configuracao['paginas_entreParaTime_servicos_hidden'] != "Esconder"):?>
		<section class="areaServicos">
			<h6>Conheça nossos serviços</h6>
			<div class="container">
				<ul>
					<?php 
						// LOOP DE POST VALORES
						$postServicos = new WP_Query( array( 'post_type' => 'servicos', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
						while ( $postServicos->have_posts() ) : $postServicos->the_post();
						if (rwmb_meta('Audiotext_linkUrlServico') != "") {$audiotext_linkUrlServico = rwmb_meta('Audiotext_linkUrlServico');}else{$audiotext_linkUrlServico = get_permalink();}
					?>
					<li>
						<!-- ÍCONE  -->
						<i class="<?php echo rwmb_meta('Audiotext_iconeServico')  ?>" style="background:<?php echo rwmb_meta('Audiotext_backgroundServico')  ?>"></i>
						<!-- TÍTULO -->
						<h2><?php echo get_the_title() ?></h2>
						<!-- DESCRIÇÃO -->
						<p><?php echo get_the_content() ?></p>
						<!-- LINK -->
						<a href="<?php  echo $audiotext_linkUrlServico ?>">Saiba mais</a>
					</li>
					<?php endwhile; wp_reset_query(); ?>
				</ul>
			</div>
		</section>
		<?php endif; ?>	

		<?php if ($configuracao['paginas_entreParaTime_um_texter_hidden'] != "Esconder"):?>
		<div class="areaSejaumtexter">
			<p><?php echo $configuracao['opt_inicial_seja_um_texter'] ?></p>
			<a href="<?php echo $configuracao['opt_inicial_seja_um_texter_btn_link'] ?>"><?php echo $configuracao['opt_inicial_seja_um_texter_btn'] ?></a>
		</div>
		<?php endif; ?>
	</div>

<?php get_footer(); ?>