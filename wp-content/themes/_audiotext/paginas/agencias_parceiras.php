

<?php
/**
 * Template Name: Agências Parceiras 
 * Description:
 *
 * @package Audiotext
 */

get_header(); ?>
<title><?php  echo get_the_title() ?> </title>
	
	<!-- PÁGINA AGÊNCIAS -->
	<div class="pg pg-agencias">
		<div class="bannerAgencia" style="background:url(<?php echo $configuracao['paginas_agencias_banner']['url'] ?>)" >
			<div class="texto">
				<span><?php echo $configuracao['paginas_agencias_titulo'] ?></span>
 			</div>
		</div>

		<section class="infoAgencia">
			<?php 
				$i = 0;
				// LOOP DE POST VALORES
				$posts = new WP_Query( array( 'post_type' => 'valoresAgencias', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
				while ( $posts->have_posts() ) : $posts->the_post();
				$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$fotoPost = $fotoPost[0];
				if($i % 2 == 0):
			?>	
			<div class="info">
				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<div class="texto">
								<h2><?php echo get_the_title() ?></h2>
								<?php echo the_content() ?>
							</div>
						</div>
						<div class="col-sm-6">
							<figure>
								<img alt="<?php echo get_the_title() ?>" title="<?php echo get_the_title() ?>" src="<?php echo $fotoPost ?>">
							</figure>
						</div>
					</div>
				</div>
			</div>
			<?php else: ?>
			<div class="info">
				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<figure>
								<img alt="<?php echo get_the_title() ?>" title="<?php echo get_the_title() ?>" src="<?php echo $fotoPost ?>">
							</figure>
						</div>
						<div class="col-sm-6">
							<div class="texto">
								<h2><?php echo get_the_title() ?></h2>
								<?php echo the_content() ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php endif;$i++; endwhile; wp_reset_query(); ?>
		</section>

		<?php if ($configuracao['paginas_agencia_carrossel_hidden'] != "Esconder"):?>
		<section class="carrosselClientes">
			<div class="container">
				<h6>Agências que já estão com a Audiotext</h6>

				<div  class="owl-Carousel" id="carrosselAgencias">
					<?php 
						$i = 0;
						// LOOP DE POST VALORES
						$posts = new WP_Query( array( 'post_type' => 'agencias', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
						while ( $posts->have_posts() ) : $posts->the_post();
						$fotoAgencias = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoAgencias = $fotoAgencias[0];
						
					?>	
					<div class="item">
						<img alt="<?php echo get_the_title() ?>" title="<?php echo get_the_title() ?>" src="<?php echo $fotoAgencias ?>">
					</div>
					<?php  endwhile; wp_reset_query(); ?>
				</div>
			</div>
		</section>
		<?php endif; ?>	

		<div class="container">
			<div class="formularioOrcamento">
				<h6>Quero ser parceiro</h6>
				<div class="row">
					<div class="col-sm-12">
					
						<div class="areaInput">
							<?php echo do_shortcode($configuracao['paginas_agencias_formulario_shortcode']); ?>
						</div>
					
					</div>
					
				</div>
			</div>
		</div>

		<!-- ÁREA SERVIÇOS -->
		<?php if ($configuracao['paginas_agencia_servicos_hidden'] != "Esconder"):?>
		<section class="areaServicos">
			<h6>Conheça nossos serviços</h6>
			<div class="container">
				<ul>
					<?php 
						// LOOP DE POST VALORES
						$postServicos = new WP_Query( array( 'post_type' => 'servicos', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
						while ( $postServicos->have_posts() ) : $postServicos->the_post();
						if (rwmb_meta('Audiotext_linkUrlServico') != "") {$audiotext_linkUrlServico = rwmb_meta('Audiotext_linkUrlServico');}else{$audiotext_linkUrlServico = get_permalink();}
					?>
					<li>
						<!-- ÍCONE  -->
						<i class="<?php echo rwmb_meta('Audiotext_iconeServico')  ?>" style="background:<?php echo rwmb_meta('Audiotext_backgroundServico')  ?>"></i>
						<!-- TÍTULO -->
						<h2><?php echo get_the_title() ?></h2>
						<!-- DESCRIÇÃO -->
						<p><?php echo get_the_content() ?></p>
						<!-- LINK -->
						<a href="<?php  echo $audiotext_linkUrlServico ?>">Saiba mais</a>
					</li>
					<?php endwhile; wp_reset_query(); ?>
				</ul>
			</div>
		</section>
		<?php endif; ?>	

		<?php if ($configuracao['paginas_agencia_um_texter_hidden'] != "Esconder"):?>
		<div class="areaSejaumtexter">
			<p><?php echo $configuracao['opt_inicial_seja_um_texter'] ?></p>
			<a href="<?php echo $configuracao['opt_inicial_seja_um_texter_btn_link'] ?>"><?php echo $configuracao['opt_inicial_seja_um_texter_btn'] ?></a>
		</div>
		<?php endif; ?>	

	</div>

	

<?php get_footer(); ?>