<?php
/**
* Template Name: Tradução Juramentada
* Description: 
*
* @package audiotext
*/
get_header(); ?>
	<title><?php echo get_the_title() ?></title>
	 <div class="pg pg-traducaoJuramentada">
	 	
	 	<!-- BANNER TOPO -->
	 	<?php if ($configuracao['paginas_traducaoJuramentada_banner_hidden'] != "Esconder"):?>
		<div class="bannerBackgroud" style="background:url(<?php echo $configuracao['paginas_traducaoJuramentada_banner']['url'] ?>)">
			<div class="areatexto">
				<h1><?php echo $configuracao['paginas_traducaoJuramentada_titulo'] ?></h1>
				<p><?php echo $configuracao['paginas_traducaoJuramentada_descricao'] ?></p>
			</div>
		</div>
		<?php endif; ?>

		<!-- SEVIÇOS -->
		<?php if ($configuracao['paginas_traducaoJuramentada_documentos_hidden'] != "Esconder"):?>
		<section class="servicos">
			<h6 class="hidden">Trdução de documentos</h6>
			<ul>
				<?php 
					$sevicos = $configuracao['paginas_traducaoJuramentada_documentos_documento'];
					foreach ($sevicos as $sevicos): $sevico = $sevicos; 
				 ?>
				<li>	
					<i class=" fa fa-file"></i>
					<h2><?php echo $sevico ?></h2>
				</li>
				<?php endforeach; ?>
			</ul>
			
		</section>
		<?php endif; ?>

		<!-- IDIOMAS -->
		<?php if ($configuracao['paginas_traducaoJuramentada_idiomas_hidden'] != "Esconder"):?>
		<section class="servicos">
			<div class="container">
				<h3><?php echo $configuracao['paginas_traducaoJuramentada_idiomas_titulo'] ?></h3>
				<p><?php echo $configuracao['paginas_traducaoJuramentada_idiomas_descricao'] ?></p>
			</div>	
			<ul>
				<?php 
					$idiomas = $configuracao['paginas_traducaoJuramentada_idiomas_idiomas'];
					foreach ($idiomas as $idiomas): $idioma= $idiomas; 
				 ?>
				<li>	
					<i class=" fa fa-flag"></i>
					<h2><?php echo $idioma?></h2>
				</li>
				<?php endforeach; ?>
			</ul>
			
		</section>
		<?php endif; ?>

		<!-- ÁREA CLIENTES -->
		<?php if ($configuracao['paginas_traducaoJuramentada_idiomas_clientes'] != "Esconder"):?>
		<section class="areaClientes">
			<h6><?php echo $configuracao['paginas_traducaoJuramentada_clientes_titulo'] ?></h6>
			<img alt="<?php echo $configuracao['paginas_traducaoJuramentada_clientes_titulo'] ?>" title="<?php echo $configuracao['paginas_traducaoJuramentada_clientes_titulo'] ?>" src="<?php echo $configuracao['paginas_traducaoJuramentada_idiomas_img']['url'] ?>" class="img-responsive">
		</section>
		<?php endif; ?>

		<!-- FORMULÁRIO -->
		<?php if ($configuracao['paginas_traducaoJuramentada_areaFomulario_hidden'] != "Esconder"):?>
		<div class="container">
			<div class="formularioOrcamento">

				<span><?php echo $configuracao['paginas_traducaoJuramentada_formulario_title'] ?> </span>
				<strong><?php echo $configuracao['paginas_traducaoJuramentada_formulario_descricao'] ?></strong>

				<div class="row">
					<?php if ($configuracao['paginas_traducaoJuramentada_formulario_hidden'] != "Esconder"):?>
					<div class="col-sm-6">

						<div class="areaInput">
							<?php echo do_shortcode($configuracao['paginas_traducaoJuramentada_formulario_shortcode']); ?>
						</div>
					</div>
				<?php endif; ?>
					<div class="col-sm-6">
						
						<div class="areaRecomendacoes">
							<?php if ($configuracao['paginas_traducaoJuramentada_depoimentos_hidden'] != "Esconder"):?>
							<p>Reconhecimento e Satisfação</p>
							<section class="areaDepoimentos">
								<h6 class="hidden">Depoimentos</h6>
								<div  class="owl-Carousel" id="carrosselDepoimentos">
									<?php 
										// LOOP COMO DEPOIMENTOS				
										$postDepoimentos = new WP_Query(array(
											'post_type'     => 'depoimentos',
											'posts_per_page'   => -1,
											'tax_query'     => array(
												array(
													'taxonomy' => 'categoriaDepoimentos',
													'field'    => 'slug',
													'terms'    => $post_slug=$post->post_name,
													)
												)
											)
										);

										// LOOP DE DESTAQUE DA CATEGORIA MARCADA
										$i = 1;
										while ( $postDepoimentos->have_posts() ) : $postDepoimentos->the_post();
				 					?>	
									<div class="item">
										<ul>
											<li>
												<i class="fa fa-quote-left "></i>

												<p><?php echo rwmb_meta('Audiotext_depoimento') ?></p>
												<span><?php echo get_the_title() ?></span>
												<?php 
												if ($urlIconeComoFunciona = rwmb_meta('Audiotext_logoComoFunciona')):
													foreach ($urlIconeComoFunciona as $urlIconeComoFunciona):
														$logoDepoimentos = $urlIconeComoFunciona;
											?>
											<!-- LOGO -->
											<img alt="<?php echo get_the_title() ?>" title="<?php echo get_the_title() ?>" src="<?php echo $logoDepoimentos['full_url'] ?>" class="img-responsive">
											<?php endforeach;endif; ?>
											</li>
										</ul>
									</div>
									<?php  $i++; endwhile; wp_reset_query();  ?>
								</div>
							</section>
							<?php endif; ?>

							<?php if ($configuracao['paginas_traducaoJuramentada_flexibilidade_audiotext_hidden'] != "Esconder"):?>
							<p><?php echo $configuracao['paginas_inicial_valores_audiotext_titulo'] ?></p>
							<section class="areaInfo">
								<h6 class="hidden">Por que confiar na Audiotext?</h6>
								<ul>

									<li>
										<i class="<?php echo $configuracao['paginas_inicial_info_flexibilidade_audiotext_icone_1'] ?>"></i>
										<strong><?php echo $configuracao['paginas_inicial_info_flexibilidade_audiotext_titulo_1'] ?></strong>
										<p><?php echo $configuracao['paginas_inicial_info_flexibilidade_audiotext_desc_1'] ?></p>
									</li>

									<li>
										<i class="<?php echo $configuracao['paginas_inicial_info_flexibilidade_audiotext_icone_2'] ?> "></i>
										<strong><?php echo $configuracao['paginas_inicial_info_flexibilidade_audiotext_titulo_2'] ?></strong>
										<p><?php echo $configuracao['paginas_inicial_info_flexibilidade_audiotext_desc_2'] ?></p>	
									</li>

									<li>
										<i class="<?php echo $configuracao['paginas_inicial_info_flexibilidade_audiotext_icone_3'] ?>"></i>
										<strong><?php echo $configuracao['paginas_inicial_info_flexibilidade_audiotext_titulo_3'] ?></strong>
										<p><?php echo $configuracao['paginas_inicial_info_flexibilidade_audiotext_desc_3'] ?></p>
									</li>

								</ul>
							</section>
							<?php endif; ?>
						</div>
					
					</div>
				</div>
				
			</div>
		</div>
		<?php endif; ?>

		<!-- ÁREA VALORES -->
		<?php if ($configuracao['paginas_traducaoJuramentada_degravacaovalores_audiotext_hidden'] != "Esconder"):?>
		<section class="areaValores">
			<h6><?php echo $configuracao['paginas_inicial_valores_audiotext_titulo'] ?></h6>
			<div class="container">
				
				<ul>
					<?php 
						// LOOP DE POST VALORES
						$posts = new WP_Query( array( 'post_type' => 'porque-confiar', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
						while ( $posts->have_posts() ) : $posts->the_post();
					?>		
					<li>
						<i class="<?php echo rwmb_meta('Audiotext_iconeConfiar') ?>" aria-hidden="true"></i>
						<span><?php echo get_the_title() ?></span>
						<p><?php echo rwmb_meta('Audiotext_textoConfiar')  ?></p>
					</li>
					<?php endwhile; wp_reset_query(); ?>
				</ul>
			</div>
		</section>
		<?php endif; ?>	

		<!-- ÁREA DEPOIMENTOS  -->
		<?php if ($configuracao['paginas_traducaoJuramentada_depoimentos_hidden'] != "Esconder"):?>
		<section class="areaDepoimentos">
			<h6><?php echo $configuracao['paginas_inicial_depoimentos_titulo'] ?></h6>
			<div class="container">
				<ul>
					<?php 
						// LOOP COMO DEPOIMENTOS				
						$postDepoimentos = new WP_Query(array(
							'post_type'     => 'depoimentos',
							'posts_per_page'   => -1,
							'tax_query'     => array(
								array(
									'taxonomy' => 'categoriaDepoimentos',
									'field'    => 'slug',
									'terms'    => $post_slug=$post->post_name,
									)
								)
							)
						);

						// LOOP DE DESTAQUE DA CATEGORIA MARCADA
						$i = 1;
						while ( $postDepoimentos->have_posts() ) : $postDepoimentos->the_post();
 					?>					
					<li>
						<i class="fa fa-quote-left"></i>
					
						<!-- DEPOIMENTOS -->
						<p><?php echo rwmb_meta('Audiotext_depoimento') ?></span>
							<?php 
							if ($urlIconeComoFunciona = rwmb_meta('Audiotext_logoComoFunciona')):
								foreach ($urlIconeComoFunciona as $urlIconeComoFunciona):
									$logoDepoimentos = $urlIconeComoFunciona;
						?>
						<!-- LOGO -->
						<img alt="<?php echo get_the_title() ?>" title="<?php echo get_the_title() ?>" src="<?php echo $logoDepoimentos['full_url'] ?>" class="img-responsive">
						<?php endforeach;endif; ?>
					
					</li>
					<?php  $i++; endwhile; wp_reset_query();  ?>

				</ul>
			</div>
		</section>
		<?php endif; ?>	

		<!-- ÁREA COMO FUNCIONA -->
		<?php if ($configuracao['paginas_traducaoJuramentada_como_funciona_hidden'] != "Esconder"):?>
		<section class="areaComoFunciona">
			<h6>Como funciona?</h6>
			<div class="container">
				<div class="row">
					<?php 
						// LOOP COMO FUNCIONA				
						$postComoFunciona = new WP_Query(array(
							'post_type'     => 'como-funciona',
							'posts_per_page'   => -1,
							'tax_query'     => array(
								array(
									'taxonomy' => 'categoriacomoFunciona',
									'field'    => 'slug',
									'terms'    => $post_slug=$post->post_name,
									)
								)
							)
						);

						$i = 1;
						while ( $postComoFunciona->have_posts() ) : $postComoFunciona->the_post();
 					?>					
					<div class="col-sm-3">
						<div class="iconeTexto">
							<?php 
								if ($urlIconeComoFunciona = rwmb_meta('Audiotext_iconeComoFunciona')):
									foreach ($urlIconeComoFunciona as $urlIconeComoFunciona):
										$iconeComoFunciona = $urlIconeComoFunciona;
							?>
							<!-- ÍCONE -->
							<img alt="<?php echo get_the_title() ?>" title="<?php echo get_the_title() ?>" src="<?php echo $iconeComoFunciona['full_url'] ?>" class="img-responsive">
							<?php endforeach;endif; ?>
							<!-- TÍTULO -->
							<span><b><?php echo $i ?> °</b><?php echo get_the_title() ?> </span>
							<!-- DESCRIÇÃO -->
							<p><?php echo rwmb_meta('Audiotext_descricaoComoFunciona'); ?></p>
						</div>
					</div>
					<?php  $i++; endwhile; wp_reset_query();  ?>
				</div>
				
			</div>
		</section>
		<?php endif; ?>	

		<!-- ÁREA DE SERVIÇOS -->
		<?php if ($configuracao['paginas_traducaoJuramentada_servicos_audiotext_hidden'] != "Esconder"):?>
		<section class="areaServicos">
			<h6><?php echo $configuracao['paginas_inicial_servicos_audiotext_titulo'] ?></h6>
			<div class="container">
				<ul>
					<?php 
						// LOOP DE POST VALORES
						$postServicos = new WP_Query( array( 'post_type' => 'servicos', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
						while ( $postServicos->have_posts() ) : $postServicos->the_post();
						if (rwmb_meta('Audiotext_linkUrlServico') != "") {$audiotext_linkUrlServico = rwmb_meta('Audiotext_linkUrlServico');}else{$audiotext_linkUrlServico = get_permalink();}
					?>
					<li>
						<!-- ÍCONE  -->
						<i class="<?php echo rwmb_meta('Audiotext_iconeServico')  ?>" style="background:<?php echo rwmb_meta('Audiotext_backgroundServico')  ?>"></i>
						<!-- TÍTULO -->
						<h2><?php echo get_the_title() ?></h2>
						<!-- DESCRIÇÃO -->
						<p><?php echo get_the_content() ?></p>
						<!-- LINK -->
						<a href="<?php  echo $audiotext_linkUrlServico ?>">Saiba mais</a>
					</li>
					<?php endwhile; wp_reset_query(); ?>
				</ul>
			</div>
		</section>
		<?php endif; ?>	

		<!-- ÁREA QUEM SOMOS FOOTER -->
		<?php if ($configuracao['paginas_traducaoJuramentada_quem_somos_footer_hidden'] != "Esconder"):?>
		<div class="areaQuemSomosfooter">
			<div class="container">
				<div class="row">

					<div class="col-md-6">
						<div class="areaTexto">

							<span><?php echo $configuracao['opt_inicial_quem_somos_titulo'] ?></span>

							<?php echo $configuracao['opt_inicial_quem_somos_texto_footer'] ?>

							<!-- BTN LINK -->
							<?php if ($configuracao['opt_inicial_quem_somos_btn_footer']):?>
							<a href="<?php echo $configuracao['opt_inicial_quem_somos_btn_link_footer'] ?>" class="button"><?php echo $configuracao['opt_inicial_quem_somos_btn_footer'] ?></a>
							<?php endif;?>

						</div>
					</div>
					<div class="col-md-6"></div>

				</div>
			</div>

		</div>
		<?php endif; ?>	

		<?php if ($configuracao['paginas_traducaoJuramentada_um_texter_hidden'] != "Esconder"):?>
		<div class="areaSejaumtexter">
			<p><?php echo $configuracao['opt_inicial_seja_um_texter'] ?></p>
			<a href="<?php echo $configuracao['opt_inicial_seja_um_texter_btn_link'] ?>"><?php echo $configuracao['opt_inicial_seja_um_texter_btn'] ?></a>
		</div>
		<?php endif; ?>

	</div>
<?php get_footer(); ?>