<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Audiotext
 */

get_header(); ?>

	<div class="pg pg-servicos">

		<!-- ÁREA SERVIÇOS -->
		<section class="areaServicos">
			<h6>Conheça nossos serviços</h6>
			<div class="container">
				
				<ul>
					<?php 
						// LOOP DE POST VALORES
						$postServicos = new WP_Query( array( 'post_type' => 'servicos', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
						while ( $postServicos->have_posts() ) : $postServicos->the_post();
						if (rwmb_meta('Audiotext_linkUrlServico') != "") {$audiotext_linkUrlServico = rwmb_meta('Audiotext_linkUrlServico');}else{$audiotext_linkUrlServico = get_permalink();}
					?>
					<li>
						<!-- ÍCONE  -->
						<i class="<?php echo rwmb_meta('Audiotext_iconeServico')  ?>" style="background:<?php echo rwmb_meta('Audiotext_backgroundServico')  ?>"></i>
						<!-- TÍTULO -->
						<h2><?php echo get_the_title() ?></h2>
						<!-- DESCRIÇÃO -->
						<p><?php echo get_the_content() ?></p>
						<!-- LINK -->
						<a href="<?php  echo $audiotext_linkUrlServico ?>">Saiba mais</a>
					</li>
					<?php endwhile; wp_reset_query(); ?>
				</ul>
			</div>
		</section>

		<!-- ÁREA CLIENTES -->
		<section class="areaClientes">
			<h6><?php echo $configuracao['paginas_inicial_logo_titulo'] ?></h6>
			<img alt="<?php echo $configuracao['paginas_inicial_logo_titulo'] ?>" title="<?php echo $configuracao['paginas_inicial_logo_titulo'] ?>" src="<?php echo $configuracao['paginas_inicial_logo_img']['url'] ?>" class="img-responsive">
		</section>


		<div class="container">

			<div class="formularioOrcamento">
				<h6>Envie sua mensagem</h6>
				<div class="row">
					<div class="col-sm-12">
					
						<div class="areaInput">
							<?php echo do_shortcode($configuracao['paginas_contato_formulario']); ?>
						</div>
					
					</div>
					
				</div>
 
			</div>
		</div>

		<!-- ÁREA DEPOIMENTOS  -->
		<section class="areaDepoimentos" >
			<h6 id="depoimentos"><?php echo $configuracao['paginas_inicial_depoimentos_titulo'] ?></h6>
			<div class="container">
				<ul>
					<?php 
						// LOOP COMO DEPOIMENTOS				
						$postDepoimentos = new WP_Query(array(
							'post_type'     => 'depoimentos',
							'posts_per_page'   => -1,
							'tax_query'     => array(
								array(
									'taxonomy' => 'categoriaDepoimentos',
									'field'    => 'slug',
									'terms'    => 'depoimentos-pagina-inicial',
									)
								)
							)
						);

						// LOOP DE DESTAQUE DA CATEGORIA MARCADA
						$i = 1;
						while ( $postDepoimentos->have_posts() ) : $postDepoimentos->the_post();
 					?>					
					<li>
						<i class="fa fa-quote-left"></i>
						<?php 
							if ($urlIconeComoFunciona = rwmb_meta('Audiotext_logoComoFunciona')):
								foreach ($urlIconeComoFunciona as $urlIconeComoFunciona):
									$logoDepoimentos = $urlIconeComoFunciona;
						?>
						<!-- LOGO -->
						<img alt="<?php echo get_the_title() ?>" title="<?php echo get_the_title() ?>" src="<?php echo $logoDepoimentos['full_url'] ?>" class="img-responsive">
						<?php endforeach;endif; ?>
						<!-- DEPOIMENTOS -->
						<p><?php echo rwmb_meta('Audiotext_depoimento') ?></span>
					</li>
					<?php  $i++; endwhile; wp_reset_query();  ?>

				</ul>
			</div>
		</section>


		<section class="servicos">
			<div class="container">
				<h3>Conheça todos os idiomas que trabalhamos</h3>
				<p>Possuímos tradutores nativos nos seguintes idiomas:</p>
			</div>	
			<ul>
				<?php 
					$idiomas = $configuracao['paginas_traducaoJuramentada_idiomas_idiomas'];
					foreach ($idiomas as $idiomas): $idioma= $idiomas; 
				 ?>
				<li>	
					<i class=" fa fa-flag"></i>
					<h2><?php echo $idioma?></h2>
				</li>
				<?php endforeach; ?>
			</ul>
			
		</section>

		<!-- ÁREA VALORES -->
		<section class="areaValores">
			<h6><?php echo $configuracao['paginas_inicial_valores_audiotext_titulo'] ?></h6>
			<div class="container">
				
				<ul>
					<?php 
						// LOOP DE POST VALORES
						$posts = new WP_Query( array( 'post_type' => 'porque-confiar', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
						while ( $posts->have_posts() ) : $posts->the_post();
					?>		
					<li>
						<i class="<?php echo rwmb_meta('Audiotext_iconeConfiar') ?>" aria-hidden="true"></i>
						<span><?php echo get_the_title() ?></span>
						<p><?php echo rwmb_meta('Audiotext_textoConfiar')  ?></p>
					</li>
					<?php endwhile; wp_reset_query(); ?>
				</ul>
			</div>
		</section>

		<!-- ÁREA QUEM SOMOS FOOTER -->
		<div class="areaQuemSomosfooter">
			<div class="container">
				<div class="row">

					<div class="col-md-6">
						<div class="areaTexto">

							<span><?php echo $configuracao['opt_inicial_quem_somos_titulo'] ?></span>

							<?php echo $configuracao['opt_inicial_quem_somos_texto_footer'] ?>

							<!-- BTN LINK -->
							<?php if ($configuracao['opt_inicial_quem_somos_btn_footer']):?>
							<a href="<?php echo $configuracao['opt_inicial_quem_somos_btn_link_footer'] ?>" class="button"><?php echo $configuracao['opt_inicial_quem_somos_btn_footer'] ?></a>
							<?php endif;?>

						</div>
					</div>
					<div class="col-md-6"></div>

				</div>
			</div>

		</div>

		<div class="areaSejaumtexter">
			<p><?php echo $configuracao['opt_inicial_seja_um_texter'] ?></p>
			<a href="<?php echo $configuracao['opt_inicial_seja_um_texter_btn_link'] ?>"><?php echo $configuracao['opt_inicial_seja_um_texter_btn'] ?></a>
		</div>

	</div>

<?php
get_footer();
