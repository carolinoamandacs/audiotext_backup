<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Audiotext
 */
	global $configuracao;

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<!-- META -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta property="og:title" content="" />
	<meta property="og:description" content="" />
	<meta property="og:url" content="" />
	<meta property="og:image" content=""/>
	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="" />


	<meta name="author" content="Audiotext">
    <meta name="keywords" content="Audiotext, Audiotext curitiba, transcrição de audio, transcrição de audio para texto online, transcrição de audio para texto em portugues, transcrição de audio preço, transcrição de audio em texto, transcrição de audio para texto preço, transcrição de audio programa, transcrição de audio para texto, transcrição de audio software, transcrição de audio download, degravação de audio para texto, traducao juramentada">
   
    <!-- <meta name="description" content=""> -->

	<meta property="og:title" content="Audiotext" />
	<meta property="og:description" content="Audiotext Transcrição de Áudio - Transcrever áudio para texto com profissionais especializados. Confira!" />
	<meta property="og:url" content="<?php echo get_site_url() ?>" />
	<meta property="og:image" content="<?php echo get_site_url() ?>/compartilhar.png"/>
	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="Audiotext" />

	<meta name="twitter:card" content="summary" />
	<meta name="twitter:site" content="<?php echo get_site_url() ?>" />
	<meta name="twitter:title" content="Audiotext Transcrição de Áudio" />
	<meta name="twitter:description" content="Audiotext Transcrição de Áudio - Transcrever áudio para texto com profissionais especializados. Confira!" />
	<meta name="twitter:image" content="<?php echo get_site_url() ?>/compartilhar.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="canonical" href="<?php echo get_site_url() ?>" />
	
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/img/ico.png">

    <!--  Favicon -->
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/Ícone-Audiotext.png">

	<!-- TÍTULO -->
	<title>Audiotext - Transcrição de áudio em texto. </title>
	<?php echo $configuracao['scripts_topo_head']  ?>
	<?php wp_head();  ?>
</head>

<body id="body" <?php body_class(); ?>>

	<?php echo $configuracao['scripts_topo_body']  ?>
	<header class="topo fixed" id="topo">
		
		<div class="container containerFull">
			<div class="row">
				<div class="col-md-2">
					<div class="logo">
						<a href="<?php echo home_url('/'); ?>">
							<strong itemprop="name">Audiotext</strong>
						</a>
					</div>
				</div>
				<div class="col-md-10">
					<div class="areaMenu">
						<!-- MENU  -->		
						<div class="navbar" role="navigation">	

							<!-- MENU MOBILE TRIGGER -->
							<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop" data-toggle="collapse" data-target="#collapse">
								<span class="sr-only"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>

							<!--  MENU MOBILE-->
							<div class="row navbar-header">			

								<nav class="collapse navbar-collapse" id="collapse">
									<?php 
										$menuAudioText = array(
											'theme_location'  => '',
											'menu'            => 'Menu Topo AudioText',
											'container'       => false,
											'container_class' => '',
											'container_id'    => '',
											'menu_class'      => 'nav navbar-nav',
											'menu_id'         => '',
											'echo'            => true,
											'fallback_cb'     => 'wp_page_menu',
											'before'          => '',
											'after'           => '',
											'link_before'     => '',
											'link_after'      => '',
											'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
											'depth'           => 2,
											'walker'          => ''
											);
										wp_nav_menu( $menuAudioText );
									?>
								</nav>						
							</div>			
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	

	
	<a href="#body" class="ancoraTopo"><img src="<?php bloginfo('template_directory'); ?>/img/backtotop.png"></a>