<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Audiotext
 */

get_header(); 
?>
<style>
		ul {
			list-style: none!important;
		}
		ul#gform_fields_1 {
			padding: 0;
		}
		form .voltarPaginaAnterior {
			display: block;
			position: absolute;
			top: 49px;
			left: 38px;
			font-size: 40px;
			color: #2a3557;
			width: 100%;
			max-width: 35px;
			text-decoration: none;
		}
		form .voltarPaginaAnterior i{
			font-family: FontAwesome;
			font-style: normal;
		}
		form .voltarPaginaAnterior:hover {
			color: #962c26;
			transition: color .4s;
			-webkit-transition: color .4s;
			-moz-transition: color .4s;
			-o-transition: color .4s;
		}

		.gform_wrapper input:not([type=radio]):not([type=checkbox]):not([type=submit]):not([type=button]):not([type=image]):not([type=file]) {
	        display: block;
		    width: 100%!important;
		    max-width: 550px;
		    background: #f7f7f7;
		    color: #34495E;
		    padding: 0 12px!important;
		    height: 40px;
		    border-color: #2f3b61;
		    border: none;
		    border-bottom: solid 1px;
		    outline: none;
		    font-size: 15px!important;
		    margin: 0 auto 35px;
		}
		.gform_wrapper .top_label .gfield_label{
			display: none!important;
		}
		.gform_wrapper form {
			text-align: center;
		}
		.gform_wrapper .gform_fileupload_multifile .gform_drop_area {
			width: 100%;
			max-width: 550px;
			margin: 0 auto;
		}
		.gform_wrapper .gform_footer input[type=submit] {
		    display: block;
		    width: 100%;
		    max-width: 230px;
		    font-weight: bold;
		    font-size: 18px!important;
		    background: #2a3557 !important;
		    height: 45px!important;
		    border: 1px solid transparent!important;
		    color: #fff!important;
		    margin: 10px auto;
		    border-radius: 3px!important;
		    outline: none!important;
		}
		.gform_wrapper .gform_footer input[type=submit]:hover{
			background: #fff!important;
			color: #2a3557!important;
			border: 1px solid #2a3557!important;
			transition: all linear .3s!important;
			-moz-transition: all linear 0.3s!important;
			-webkit-transition: all linear 0.3s!important;
			-o-transition: all linear 0.3s!important;
			border-radius: 3px!important;
		}
		.entry-footer,
		.entry-header {
			display: none!important;
		}
		#primary {
			padding-top: 70px;
		}

	</style>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

