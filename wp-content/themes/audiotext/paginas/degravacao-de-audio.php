<?php
/**
* Template Name: Degravação de Áudio em Texto
* Description: 
*
* @package audiotext
*/
get_header(); ?>
<title><?php echo get_the_title() ?></title>
<div class="pg pg-inicial">

	<?php if ($configuracao['opt_degravacaoquem_somos_hidden'] != "Esconder"):?>
	<section class="areaSobreEmpresa">
		<h6 class="hidden">Sobre a Audiotext</h6>
		<div class="container containerConteudoFull">
			<div class="row">

			<?php if ($configuracao['opt_degravacaoquem_somos_video']):?>
				<div class="col-sm-5">
					<div class="areaTexto">
						<?php echo $configuracao['opt_degravacaoquem_somos_texto'] ?>
						<?php if ($configuracao['opt_degravacaoquem_somos_btn']):?>
						<a href="#" class="button abrirFormularioDeOrcamento"><?php echo $configuracao['opt_transcricao_quem_somos_btn'] ?></a>
						<?php endif;?>
					</div>
				</div>

				<div class="col-sm-7">
					<div class="areaCarrossel">
						<div  class="carrosselDestaque">
							<div class="areaVideo">
								<iframe src="https://player.vimeo.com/video/<?php echo $configuracao['opt_degravacaoquem_somos_video'] ?>" width="640" height="360" frameborder="0" allowfullscreen></iframe>
							</div>
						</div>
					</div>
				</div>
					
			<?php else: ?>
				<div class="col-sm-12 text-center">
					<div class="areaTexto">
						<?php echo $configuracao['opt_degravacaoquem_somos_texto'] ?>
						<?php if ($configuracao['opt_degravacaoquem_somos_btn']):?>
							<a href="<?php echo $configuracao['opt_degravacaoquem_somos_btn_link'] ?>" class="button abrirFormularioDeOrcamento"><?php echo $configuracao['opt_transcricao_quem_somos_btn'] ?></a>
						<?php endif;?>
					</div>
				</div>
			<?php endif; ?>
			</div>
		</div>
	</section>
	<?php endif; ?>

	<?php if ($configuracao['paginas_inicial_info_audiotext_hidden'] != "Esconder"):?>
	<section class="areaInfoServicos" style="display: none;">
		<h6 class="hidden">Informações serviços</h6>
		<div class="container">
			<p><?php echo $configuracao['paginas_transcricao_informativo_texto'] ?></p>
		</div>
	</section>
	<?php endif; ?>

	<?php if ($configuracao['opt_inicial_como_funciona_hidden'] != "Esconder"):?>
		<section class="areaComoFunciona" id="comofunciona">
			<h6>Como funciona?</h6>
			<div class="container">
				<div class="row">
					<?php 
					// LOOP COMO FUNCIONA				
					$postComoFunciona = new WP_Query(array(
							'post_type'     => 'como-funciona',
							'posts_per_page'   => -1,
							'tax_query'     => array(
								array(
									'taxonomy' => 'categoriacomoFunciona',
									'field'    => 'slug',
									'terms'    => 'pagina-inicial',
								)
							)
						)
					);
					$i = 1;
					while ( $postComoFunciona->have_posts() ) : $postComoFunciona->the_post();
						?>
						<div class="col-sm-3">
							<div class="iconeTexto">
								<?php 
								if ($urlIconeComoFunciona = rwmb_meta('Audiotext_iconeComoFunciona')):
									foreach ($urlIconeComoFunciona as $urlIconeComoFunciona):
										$iconeComoFunciona = $urlIconeComoFunciona;
										?>
										<img alt="<?php echo get_the_title() ?>" title="<?php echo get_the_title() ?>" src="<?php echo $iconeComoFunciona['full_url'] ?>" class="img-responsive">
									<?php endforeach;endif; ?>
								<span><b><?php echo $i ?> °</b><?php echo get_the_title() ?> </span>
								<p><?php echo rwmb_meta('Audiotext_descricaoComoFunciona'); ?></p>
							</div>
						</div>
							<?php  $i++; endwhile; wp_reset_query();  ?>
				</div>
						<?php if ($configuracao['opt_inicial_como_funciona_btn']):?>
							<a href="<?php echo $configuracao['opt_inicial_como_funciona_btn_link'] ?>" class="button itemSolicitarOcamento"><?php echo $configuracao['opt_inicial_quem_somos_btn'] ?></a>
						<?php endif;?>
			</div>
		</section>
	<?php endif; ?>
	<?php if ($configuracao['paginas_inicial_logo_hidden'] != "Esconder"):?>
		<section class="areaClientes" id="clientes">
			<h6><?php echo $configuracao['paginas_inicial_logo_titulo'] ?></h6>
			<div class="container">
				<div class="carrosselClientes" id="carrosselClientes">
						<?php 
						// LOOP COMO DEPOIMENTOS				
						$postClientes = new WP_Query(
							array(
								'post_type'     => 'clientes',
								'posts_per_page'   => -1,
							)
						);
						while ( $postClientes->have_posts() ) : $postClientes->the_post();
							$logoCliente = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
							$logoCliente = $logoCliente[0];

						?>
					<div class="item">
						<figure>
							<img src="<?php echo $logoCliente ?>" alt="<?php echo get_the_title(); ?>">
						</figure>
					</div>	

					<?php endwhile; ?>
				</div>
			</div>
		</section>
	<?php endif; ?>
	<?php if ($configuracao['paginas_inicial_depoimentos_hidden'] != "Esconder"):?>
		<section class="areaDepoimentos" >
			<h6 id="depoimentos"><?php echo $configuracao['paginas_inicial_depoimentos_titulo'] ?></h6>
			<div class="container">
				<div class="carrosselDepoimentos" id="carrosselDepoimentos">
					<?php 
						// LOOP COMO DEPOIMENTOS				
						$postDepoimentos = new WP_Query(array(
							'post_type'     => 'depoimentos',
							'posts_per_page'   => -1,
							'tax_query'     => array(
									array(
										'taxonomy' => 'categoriaDepoimentos',
										'field'    => 'slug',
										'terms'    => 'degravacao-de-audio-em-texto',
									)
								)
							)
						);
					
					// LOOP DE DESTAQUE DA CATEGORIA MARCADA
					$i = 1;
					while ( $postDepoimentos->have_posts() ) : $postDepoimentos->the_post();

						?>
						<div class="item">
							<i class="fa fa-quote-left"></i>
							<?php 
							if ($urlIconeComoFunciona = rwmb_meta('Audiotext_logoComoFunciona')):
								foreach ($urlIconeComoFunciona as $urlIconeComoFunciona):
									$logoDepoimentos = $urlIconeComoFunciona;
									?>
									<img alt="<?php echo get_the_title() ?>" title="<?php echo get_the_title() ?>" src="<?php echo $logoDepoimentos['full_url'] ?>" class="img-responsive">
								<?php endforeach;endif; ?>
								<p><?php echo rwmb_meta('Audiotext_depoimento') ?></span>
						</div>
					<?php  $i++; endwhile; wp_reset_query();  ?>
				</div>
			</div>
		</section>
	<?php endif; ?>
	<?php if ($configuracao['paginas_inicial_valores_audiotext_hidden'] != "Esconder"):?>
		<section class="areaValores">
			<h6><?php echo $configuracao['paginas_inicial_valores_audiotext_titulo'] ?></h6>
			<div class="container">
				<ul>
					<?php 
					// LOOP DE POST VALORES
					$posts = new WP_Query( array( 'post_type' => 'porque-confiar', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
					while ( $posts->have_posts() ) : $posts->the_post();
						?>
						<li>
							<i class="<?php echo rwmb_meta('Audiotext_iconeConfiar') ?>" aria-hidden="true"></i>
							<span><?php echo get_the_title() ?></span>
							<p><?php echo rwmb_meta('Audiotext_textoConfiar')  ?></p>
						</li>
					<?php endwhile; wp_reset_query(); ?>
				</ul>
			</div>
		</section>
	<?php endif; ?>
	<?php if ($configuracao['paginas_inicial_servicos_audiotext_hidden'] != "Esconder"):?>
		<section class="areaServicos" id="servicos">
			<h6><?php echo $configuracao['paginas_inicial_servicos_audiotext_titulo'] ?></h6>
			<div class="container">
				<ul>
					<?php 
					// LOOP DE POST VALORES
					$postServicos = new WP_Query( array( 'post_type' => 'servicos', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
					while ( $postServicos->have_posts() ) : $postServicos->the_post();
						if (rwmb_meta('Audiotext_linkUrlServico') != "") {$audiotext_linkUrlServico = rwmb_meta('Audiotext_linkUrlServico');}else{$audiotext_linkUrlServico = get_permalink();}
						?>
						<li>
							<i class="<?php echo rwmb_meta('Audiotext_iconeServico')  ?>" style="background:<?php echo rwmb_meta('Audiotext_backgroundServico')  ?>"></i>
							<h2><?php echo get_the_title() ?></h2>
							<p><?php echo get_the_content() ?></p>
							<a href="<?php  echo $audiotext_linkUrlServico ?>">Saiba mais</a>
						</li>
					<?php endwhile; wp_reset_query(); ?>
				</ul>
			</div>
		</section>
	<?php endif; ?>
	<?php if ($configuracao['paginas_inicial_info_flexibilidade_audiotext_hidden'] != "Esconder"):?>
		<section class="areaInfo">
			<h6 class="hidden">Valores Audiotext</h6>
			<div class="container">
				<ul>
					<li>
						<i class="<?php echo $configuracao['paginas_inicial_info_flexibilidade_audiotext_icone_1'] ?>"></i>
						<strong><?php echo $configuracao['paginas_inicial_info_flexibilidade_audiotext_titulo_1'] ?></strong>
						<p><?php echo $configuracao['paginas_inicial_info_flexibilidade_audiotext_desc_1'] ?></p>
					</li>
					<li>
						<i class="<?php echo $configuracao['paginas_inicial_info_flexibilidade_audiotext_icone_2'] ?>"></i>
						<strong><?php echo $configuracao['paginas_inicial_info_flexibilidade_audiotext_titulo_2'] ?></strong>
						<p><?php echo $configuracao['paginas_inicial_info_flexibilidade_audiotext_desc_2'] ?></p>
					</li>
					<li>
						<i class="<?php echo $configuracao['paginas_inicial_info_flexibilidade_audiotext_icone_3'] ?>"></i>
						<strong><?php echo $configuracao['paginas_inicial_info_flexibilidade_audiotext_titulo_3'] ?></strong>
						<p><?php echo $configuracao['paginas_inicial_info_flexibilidade_audiotext_desc_3'] ?></p>
					</li>
				</ul>
			</div>
		</section>
	<?php endif; ?>
						
	<?php if ($configuracao['opt_inicial_quem_somos_footer_hidden'] != "Esconder"):?>
		<div class="areaQuemSomosfooter">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="areaTexto">
							<span><?php echo $configuracao['opt_inicial_quem_somos_titulo'] ?></span>
							<?php echo $configuracao['opt_inicial_quem_somos_texto_footer'] ?>
							<?php if ($configuracao['opt_inicial_quem_somos_btn_footer']):?>
								<a href="<?php echo $configuracao['opt_inicial_quem_somos_btn_link_footer'] ?>" class="button ancora"><?php echo $configuracao['opt_inicial_quem_somos_btn_footer'] ?></a>
							<?php endif;?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="divImagemQuemSomos">
							<figure class="fotoEquipe" style="background: url(<?php echo $configuracao['opt_inicial_quem_somos_imagem_equipe']['url'] ?>);">
								<img src="<?php echo $configuracao['opt_inicial_quem_somos_imagem_equipe']['url'] ?>" alt="Foto da equipe da audiotext">
							</figure>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<?php if ($configuracao['paginas_inicial_seja_um_texter_hidden'] != "Esconder"):?>
		<div class="areaSejaumtexter">
			<p><?php echo $configuracao['opt_inicial_seja_um_texter'] ?></p>
			<a href="<?php echo $configuracao['opt_inicial_seja_um_texter_btn_link'] ?>" class="abrirModalEntreParaOTime" ><?php echo $configuracao['opt_inicial_seja_um_texter_btn'] ?></a>
		</div>
	<?php endif; ?>
</div>
<?php get_footer(); ?>