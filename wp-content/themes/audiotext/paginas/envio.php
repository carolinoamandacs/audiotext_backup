<?php
/**
* Template Name: Envio
* Description: Página de envio de arquivos
*
* @package audiotext
*/
 ?>
	<div class="formularioArquivos">
		<a href="http://novoaudiotext.hcdesenvolvimentos.com.br" class="voltarPaginaAnterior">
			<i class="fas fa-arrow-left"></i>
		</a>
		<div class="camposFormularioDeArquivos">
		<div class="textoInicial">
			<p>Preencha o formulário abaixo e envie seus arquivos para a Audiotext.</p>
		</div>	
			<!-- FORMULÁRIO PARA ENVIO DE ARQUIVOS -->
			<?php echo do_shortcode('[gravityform id="5" title="false" description="false" ajax="true"]'); ?>
		</div>
	</div>
	
	<style>
		.topo {
			display: none!important;
		}
		ul {
			list-style: none!important;
		}
		ul#gform_fields_1 {
			padding: 0;
		}
		.formularioArquivos .voltarPaginaAnterior {
			display: block;
			position: absolute;
			top: 49px;
			left: 38px;
			font-size: 40px;
			color: #2a3557;
			width: 100%;
			max-width: 35px;
			text-decoration: none;
		}
		.formularioArquivos .voltarPaginaAnterior i{
			font-family: FontAwesome;
			font-style: normal;
		}
		.formularioArquivos .voltarPaginaAnterior:hover {
			color: #962c26;
			transition: color .4s;
			-webkit-transition: color .4s;
			-moz-transition: color .4s;
			-o-transition: color .4s;
		}
	</style>
